module adsd.demo.ovintro {
   requires javafx.controls;
   requires javafx.fxml;
    requires java.desktop;
    requires java.sql;
    requires commons.codec;


    opens adsd.demo.ovintro to javafx.fxml;
   exports adsd.demo.ovintro;
}
