package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Objects;

public class Route
{
   private final ArrayList<StopOver> stopOvers = new ArrayList<>();

   ///////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////
   public Route(Location beginLocation, LocalTime departure )
   {
      var stopover = new StopOver( beginLocation.getName(), null, departure );
      stopOvers.add( stopover );
   }

   ///////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////
   public void addStopOver(Location loc, LocalTime arrival, LocalTime departure )
   {
      var stopover = new StopOver( loc.getName(), arrival, departure );
      stopOvers.add( stopover );
   }

   public StopOver getStopOver(String Locationkey) {
      for (var h : stopOvers) {
         if (h.getName().equals(Locationkey)) {
            return h;
         }
      }
      return null;
   }

   ///////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////
   public void addEndPoint(Location loc, LocalTime arrival )
   {
      var stopover = new StopOver( loc.getName(), arrival, null );
      stopOvers.add( stopover );
   }

   public LocalTime getDepartureTime(String name)
   {
      return Objects.requireNonNull(stopOvers.stream().filter(n -> Objects.equals(n.getName(), name)).findAny().orElse(null)).getDeparture();
   }


   ///////////////////////////////////////////////////////////////
   // Construct a key associated with a Route instance by appending
   // the names of the stopovers in this route, separated by a '-'.
   // To make the key unique, append '|' + departure time.
   ///////////////////////////////////////////////////////////////
   public String getKey()
   {
      StringBuilder key = new StringBuilder(stopOvers.get(0).getName());
      for (int i = 1; i < stopOvers.size(); i++)
      {
         key.append("-");
         key.append(stopOvers.get(i).getName());
      }
      key.append("|");
      key.append(stopOvers.get(0).getDeparture());
      return key.toString();
   }

   ///////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////
   public void write()
   {
      var first = stopOvers.get( 0 );
      var last  = stopOvers.get( stopOvers.size() - 1 );
      System.out.format( "route: %s, dep. %s at %s; arr. %s at %s\n", getKey(),
                         first.getName(), first.getDeparture(), last.getName(), last.getArrival() );
   }
}
