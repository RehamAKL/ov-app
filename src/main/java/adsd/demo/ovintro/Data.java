package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;

public  class Data {
    private final Map<String, Location> locationMap = new TreeMap<>();
    private final Map<String, Route> routeMap = new TreeMap<>();
    private final String key1;
    private final String key2;
    private final LocalTime t;

    public Data() {
        /// === Locations A ... F ========
        var location = new Location("A",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        location = new Location("B",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        location = new Location("C",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        location = new Location("D",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        location = new Location("E",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        location = new Location("F",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        ////////////////////////////////////////////////////////////

        /// === Routes A-B-C-D ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("A"), departure);
            route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("D"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);

            writeRoute(route);
        }

        /// === Routes D-C-B-A
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("D"), departure);

            route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("A"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
            writeRoute(route);
        }
        writeRoutesABCD();

        /// === Routes E-B-C-F ========
        for (int hour = 8; hour <= 17; hour += 1) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("E"), departure);

            route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 45), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("C"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("F"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
            writeRoute(route);

        }

        /// === Routes F-C-B-E ========
        for (int hour = 8; hour <= 17; hour += 1) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("F"), departure);

            route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 45), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("B"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("E"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
            writeRoute(route);
        }

        // === Route B-C ===
        for (int hour = 12; hour <= 12; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("B"), departure);

            route.addEndPoint(locationMap.get("C"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
            writeRoute(route);
        }

        // === Route C-B ===
        for (int hour = 12; hour <= 12; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("C"), departure);

            route.addEndPoint(locationMap.get("B"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
            writeRoute(route);
        }

        writeRoutesBD();
        key1 = "C";
        key2 = "B";
        t = LocalTime.of(11, 12);

        writeRoutes(key1, key2);
        writeRoutesT(key1, key2, t);
//        writeRoutesTrip(key1, key2, t);
    }

    void writeRoute(Route route) {
        System.out.println("Start writeRoute: ");
        route.write();
        System.out.println("Stop writeRoute");
    }

    public String[] getLocationsNames() {
        String[] locationsNames = new String[locationMap.size()];
        int index = 0;
        for (var e : locationMap.values()) {
            locationsNames[index++] = e.getName();
        }
        return locationsNames;
    }

    void writeRoutesABCD() {
        System.out.println("writeRoutesABCD start: ");
        for (Route r : routeMap.values()) {
            if (r.getKey().startsWith("A") && r.getKey().contains("D") && r.getKey().indexOf("A") < r.getKey().indexOf("D")) {
                r.write();
            }
        }
        System.out.println("writeRoutesABCD END");
    }

    void writeRoutesBD() {
        System.out.println("writeRoutesBD start: ");

        for (Route r : routeMap.values()) {
            if (r.getKey().contains("B") && r.getKey().contains("D") && r.getKey().indexOf("B") < r.getKey().indexOf("D")) {
                r.write();
            }
        }
        System.out.println("writeRoutesBD END");
    }

    void writeRoutes(String key1, String key2) {
        System.out.println("Start writeRoutes: ");
        for (Route r : routeMap.values()) {
            if (r.getKey().contains(key1) && r.getKey().contains(key2) && r.getKey().indexOf(key1) < r.getKey().indexOf(key2)) {
                r.write();
            }
        }
        System.out.println("End writeRoutes");
    }


    void writeRoutesT(String key1, String key2, LocalTime t) {
        System.out.println("Start writeRoutesT:");
        for (Route r : routeMap.values()) {
            if (r.getKey().contains(key1) && r.getKey().contains(key2) && r.getKey().indexOf(key1) < r.getKey().indexOf(key2)) {
                var time = r.getDepartureTime(key1);
                if (t.isBefore(time)) {
                    r.write();
                }
            }
        }
        System.out.println("End writeRoutesT");
    }
}

