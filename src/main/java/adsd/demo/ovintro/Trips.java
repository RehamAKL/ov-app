package adsd.demo.ovintro;

import java.util.ArrayList;
import java.util.Iterator;

public class Trips {
    ArrayList<Trip> tripList = new ArrayList<>();

    public void addTrip(Trip trip){
        tripList.add(trip);
    }

    public Iterator<Trip> iterator() { return tripList.iterator();}

    public void write(){
        int count = 0;
        for (var e : tripList) {
            System.out.format("%2d", count++);
            e.write();
        }
    }
}
