package adsd.demo.ovintro;

public class Location
{
   private final String name;
   private Coordinates coordinates;


   Location( String name ,Coordinates coordinates )
   {
      this.coordinates=coordinates;
      this.name = name;
   }

   public String getName()
   {
      return name;
   }

   public Coordinates getCoordinates() {
      return coordinates;
   }

   public void setCoordinates(Coordinates coordinates) {
      this.coordinates = coordinates;
   }
}
