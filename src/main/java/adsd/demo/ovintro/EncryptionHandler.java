package adsd.demo.ovintro;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

public class EncryptionHandler {
    private final int iterations = 10000;
    private final int keyLength = 512;
    public String[] encode(String password, String salt) {
        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = salt.getBytes();

        byte[] hashedBytes = hashPassword(passwordChars, saltBytes, iterations, keyLength);
        String hashedString = Hex.encodeHexString(hashedBytes);
        String[] encodedPassword = {hashedString, salt};
        return encodedPassword;
    }

    public boolean checkPassword(String password, String salt, String oldHashedString) {
        String[] newHashedString = encode(password, salt);
        return oldHashedString.equals(newHashedString[0]);
    }

    private static byte[] hashPassword( final char[] password, final byte[] salt, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            PBEKeySpec spec = new PBEKeySpec( password, salt, iterations, keyLength );
            SecretKey key = skf.generateSecret( spec );
            return key.getEncoded( );
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException( e );
        }
    }

    public String randomSalt() {
        int m = (int) Math.pow(10, keyLength - 1);
        return String.valueOf(m + new Random().nextInt(9 * m));
    }
}
