package adsd.demo.ovintro;


public class Coordinates {
    private final double latitude;
    private final double longitude;

    public Coordinates (double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getDistance(Coordinates coordinates) {
        double long1 = Math.toRadians(this.getLongitude());
        double long2 = Math.toRadians(coordinates.getLongitude());
        double latitude1 = Math.toRadians(this.getLatitude());
        double latitude2 = Math.toRadians(coordinates.getLatitude());
        //onderstaande staat de Haversine formula
        double lon1 = long2 - long1;
        double lat1 = latitude2 - latitude1;
        double a = Math.pow(Math.sin(lat1 / 2), 2)
                + Math.cos(latitude1) * Math.cos(latitude2)
                * Math.pow(Math.sin(lon1 / 2),2);

        double d = 2 * Math.asin(Math.sqrt(a));

        // om resultaten te berekenen
        int result = 6371;
        return(d * result);
    }
}
