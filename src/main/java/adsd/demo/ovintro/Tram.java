package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;

public class Tram extends Data   {
    private final Map<String, Location> locationMap = new TreeMap<>();
    private final Map<String, Route> routeMap = new TreeMap<>();
    public Map<String, Location> getLocationMap() {
        return locationMap;
    }

    public Tram() {
        /// === Locations A ... F ========
        var  location = new Location("Utrecht,P+R Science Park",new Coordinates(52.085380, 5.090670));
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht, CS Centrumzijde",new Coordinates(52.090020, 5.111070));
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht, Winkelcentrum Kanaleneiland",new Coordinates(52.070330, 5.096230));
        locationMap.put(location.getName(), location);

         location = new Location("Nieuwegein City",new Coordinates(52.032450, 5.088990));
        locationMap.put(location.getName(), location);

        location = new Location("Ijsselstijn,Binnenstad",new Coordinates(51.047860, 5.728730));
        locationMap.put(location.getName(), location);

        location = new Location("Ijsselstijn,zuid",new Coordinates(52.017765, 5.0403));
        locationMap.put(location.getName(), location);

        ////////////////////////////////////////////////////////////

        /// === Routes A-B-C-D ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);

            var route = new Route(locationMap.get("Utrecht,P+R Science Park"), departure);
            route.addStopOver(locationMap.get("Utrecht, CS Centrumzijde"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Nieuwegein City"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Nieuwegein City"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes D-C-B-A
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Nieuwegein City"), departure);

            route.addStopOver(locationMap.get("Nieuwegein City"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Utrecht, CS Centrumzijde"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Utrecht,P+R Science Park"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes E-B-C-F ========
        for (int hour = 8; hour <= 17; hour += 1) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Ijsselstijn,Binnenstad"), departure);

            route.addStopOver(locationMap.get("Utrecht, CS Centrumzijde"), LocalTime.of(hour, 45), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Nieuwegein City"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("Ijsselstijn,zuid"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes F-C-B-E ========
        for (int hour = 8; hour <= 17; hour += 1) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Ijsselstijn,zuid"), departure);

            route.addStopOver(locationMap.get("Nieuwegein City"), LocalTime.of(hour, 45), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Utrecht, CS Centrumzijde"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("Ijsselstijn,Binnenstad"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
        }

        // === Route B-C ===
        for (int hour = 12; hour <= 12; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht, CS Centrumzijde"), departure);

            route.addEndPoint(locationMap.get("Nieuwegein City"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        // === Route C-B ===
        for (int hour = 12; hour <= 12; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Nieuwegein City"), departure);

            route.addEndPoint(locationMap.get("Utrecht, CS Centrumzijde"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
    }
}
