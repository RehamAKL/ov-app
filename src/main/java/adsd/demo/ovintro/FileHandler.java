package adsd.demo.ovintro;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class FileHandler {
    private final Document doc;

    public FileHandler() throws IOException, ParserConfigurationException, SAXException {
        doc = readDocument();
    }

    private Document readDocument() throws IOException, SAXException, ParserConfigurationException {
        File f = new File("files/lang.xml");
        final DocumentBuilderFactory dbf= DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = dbf.newDocumentBuilder();
        Document readDoc = builder.parse(f);
        readDoc.getDocumentElement().normalize();
        return readDoc;
    }

    public String getObjectText(String objectName, String language) {
        NodeList nodeList = doc.getElementsByTagName(objectName);
        Element element = (Element) nodeList.item(0);
        return element.getElementsByTagName(language).item(0).getTextContent();
    }
}
