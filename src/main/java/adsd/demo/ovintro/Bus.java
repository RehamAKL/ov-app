package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;

public class Bus extends Data   {
    private final Map<String, Location> locationMap = new TreeMap<>();
    private final Map<String, Route> routeMap = new TreeMap<>();
    public Map<String, Location> getLocationMap() {
        return locationMap;
    }

    public Bus() {
        /// === Locations A ... F ========
        var location = new Location("Utrecht, Heidelberglaan",new Coordinates(52.086043, 5.181993));
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht, UMC Utrecht",new Coordinates(52.0870179, 5.180100517596717));
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht, WKZ-Maxima",new Coordinates(52.0895732, 5.1830252));
        locationMap.put(location.getName(), location);

        location = new Location("Soesterberg, P+R Soesterberg",new Coordinates(52.1169205, 5.3053722));
        locationMap.put(location.getName(), location);

        location = new Location("Amersfoort, Warande",new Coordinates(52.15066, 5.35721));
        locationMap.put(location.getName(), location);

        location = new Location("Amersfoort Centraal",new Coordinates(52.153898,5.3740976 ));
        locationMap.put(location.getName(), location);

        ////////////////////////////////////////////////////////////

        /// === Routes A-B-C-D ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht, Heidelberglaan"), departure);

            route.addStopOver(locationMap.get("Utrecht, UMC Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Utrecht, WKZ-Maxima"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Soesterberg, P+R Soesterberg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes D-C-B-A
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Soesterberg, P+R Soesterberg"), departure);

            route.addStopOver(locationMap.get("Utrecht, WKZ-Maxima"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Utrecht, UMC Utrecht"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Utrecht, Heidelberglaan"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes E-B-C-F ========
        for (int hour = 8; hour <= 17; hour += 1) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Amersfoort, Warande"), departure);

            route.addStopOver(locationMap.get("Utrecht, UMC Utrecht"), LocalTime.of(hour, 45), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Utrecht, WKZ-Maxima"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes F-C-B-E ========
        for (int hour = 8; hour <= 17; hour += 1) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Amersfoort Centraal"), departure);

            route.addStopOver(locationMap.get("Utrecht, WKZ-Maxima"), LocalTime.of(hour, 45), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Utrecht, UMC Utrecht"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("Amersfoort, Warande"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
        }

        // === Route B-C ===
        for (int hour = 12; hour <= 12; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht, UMC Utrecht"), departure);

            route.addEndPoint(locationMap.get("Utrecht, WKZ-Maxima"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        // === Route C-B ===
        for (int hour = 12; hour <= 12; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht, WKZ-Maxima"), departure);

            route.addEndPoint(locationMap.get("Utrecht, UMC Utrecht"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
    }
}
