package adsd.demo.ovintro;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;

public class Train extends Data   {
    private final Map<String, Location> locationMap = new TreeMap<>();
    private final Map<String, Route> routeMap = new TreeMap<>();
    public Map<String, Location> getLocationMap() {
        return locationMap;
    }

    public Train() {
        /// === Locations ========
        var location = new Location("Groningen",new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getName(), location);

        location = new Location("Leeuwarden",new Coordinates(52.09073739999999, 5.121420100000023));
        locationMap.put(location.getName(), location);

        location = new Location("Assen",new Coordinates(51.9244201, 4.477732500000002));
        locationMap.put(location.getName(), location);

        location = new Location("Zwolle",new Coordinates(52.1561113, 5.387826600000039));
        locationMap.put(location.getName(), location);

        location = new Location("Lelystad",new Coordinates(52.0704978, 4.3006999000000405));
        locationMap.put(location.getName(), location);

        location = new Location("Arnhem",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);

        location = new Location("Middelburg",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);

        location = new Location("den Bosch",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);

        location = new Location("Maastricht",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);

        location = new Location("Haarlem",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);

        location = new Location("den Haag",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht",new Coordinates(52.5167747, 6.083021899999949));
        locationMap.put(location.getName(), location);
        ////////////////////////////////////////////////////////////

        /// === Routes Groningen-Middelburg ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Middelburg-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 37), LocalTime.of(hour, 39));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour, 37), LocalTime.of(hour, 39));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 37), LocalTime.of(hour, 39));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Maasricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 37), LocalTime.of(hour, 39));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 37), LocalTime.of(hour, 39));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Leeuwarden-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Lelystad-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Haarlem-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes den Haag-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Middelburg-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Leeuwarden"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Leeuwarden"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("den Haag"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("den Haag"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Middelburg
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Middelburg-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Assen-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Leeuwarden"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Assen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Leeuwarden"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Utrecht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Maastricht-Lelystad
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Lelystad-Maastricht
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("den Bosch"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Zwolle
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Zwolle-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Leeuwarden-Arnhem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,35), LocalTime.of(hour,36));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Arnhem-Leeuwarden
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-den Haag
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,35), LocalTime.of(hour,36));
            route.addEndPoint(locationMap.get("den Haag"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Haag-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-Haarlem
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,35), LocalTime.of(hour,36));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Haarlem-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Groningen-den Bosch
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,35), LocalTime.of(hour,36));
            route.addEndPoint(locationMap.get("den Bosch"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes den Bosch-Groningen
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("den Bosch"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour,15), LocalTime.of(hour,16));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour,25), LocalTime.of(hour,26));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }
    }

    public Trips getTrips(String key1, String key2, LocalDateTime ldt ) {
        Trips trips = new Trips();
        for (var e : routeMap.entrySet()) {
            var key = e.getKey();
            var posA = key.indexOf(key1);
            if (posA >= 0) {
                var posB = key.indexOf(key2);
                if (posB > posA) {
                    var route = e.getValue();
                    var halte = route.getStopOver(key1);
                    assert (halte != null);
                    if (halte.getDeparture().isAfter(LocalTime.from(ldt))) {
                        Trip trip = new Trip(route, locationMap.get(key1), locationMap.get(key2), ldt);
                        trips.addTrip(trip);
                    }
                }
            }
        }
        return trips;
    }
}
