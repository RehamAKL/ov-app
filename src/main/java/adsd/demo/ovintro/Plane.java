package adsd.demo.ovintro;

import java.time.LocalTime;
import java.util.Map;
import java.util.TreeMap;

public class Plane extends Data {
    private final Map<String, Location> locationMap = new TreeMap<>();
    private final Map<String, Route> routeMap = new TreeMap<>();
    public Map<String, Location> getLocationMap() {
        return locationMap;
    }

    public Plane() {
        /// === Locations A ... F ========
        var location = new Location("Schiphol Airport",new Coordinates(52.30949795164425, 4.792265890910406));
        locationMap.put(location.getName(), location);

        location = new Location("Rotterdam Airport",new Coordinates(51.95633585, 4.439731915265206));
        locationMap.put(location.getName(), location);

        location = new Location("Eindhoven Airport",new Coordinates(51.449830000000006, 5.373710920240573));
        locationMap.put(location.getName(), location);

        location = new Location("Groningen Airport",new Coordinates(51.449830000000006, 5.373710920240573));
        locationMap.put(location.getName(), location);

        location = new Location("Maastricht Aachen Airport",new Coordinates(50.91249905,5.77132050283004 ));
        locationMap.put(location.getName(), location);

        location = new Location("Brussels Airport",new Coordinates(50.900999, 4.485574400000019));
        locationMap.put(location.getName(), location);

        ////////////////////////////////////////////////////////////

        /// === Routes A-B-C-D ========
        for (int hour = 6; hour <= 19; hour += 4) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Schiphol Airport"), departure);

            route.addStopOver(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 31), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Groningen Airport"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes D-C-B-A
        for (int hour = 6; hour <= 19; hour += 4) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen Airport"), departure);

            route.addStopOver(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 15), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 32), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 48));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes E-B-C-F ========
        for (int hour = 6; hour <= 17; hour += 4) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht Aachen Airport"), departure);

            route.addStopOver(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 45), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Eindhoven Airport"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("Brussels Airport"), LocalTime.of(hour, 18));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes F-C-B-E ========
        for (int hour = 6; hour <= 17; hour += 4) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Brussels Airport"), departure);

            route.addStopOver(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 45), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Rotterdam Airport"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 3));
            route.addEndPoint(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour + 1, 18));
            routeMap.put(route.getKey(), route);
        }

        // === Route B-C ===
        for (int hour = 7; hour <= 20; hour += 4) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Rotterdam Airport"), departure);

            route.addEndPoint(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        // === Route C-B ===
        for (int hour = 7; hour <= 19; hour += 4) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Eindhoven Airport"), departure);

            route.addEndPoint(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
    }
}

