package adsd.demo.ovintro;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TreeMap;

public class Trip {
    private final Route route;
    private final Location locationA;
    private final Location locationB;
    private final LocalDateTime dateTime;
    private final Map<String, Route> tripMap = new TreeMap<>();

    void addTrip(Route route)
    {
        tripMap.put(route.getKey(), route);
    }

    public Trip(Route route, Location lacationA, Location locationB, LocalDateTime dateTime) {
        this.route = route;
        this.locationA = lacationA;
        this.locationB = locationB;
        this.dateTime=dateTime;
    }

    public void write(){
      toString();
//        var dep = route.getStopOver(locationA.getName());
//        var arr =route.getStopOver(locationB.getName());
//        var deptime = dep.getDeparture();
//        var arrtime = arr.getArrival();
//        Duration duration = Duration.between(deptime,arrtime);
////        System.out.format("Trip:",lacationA, route, locationB);
//        System.out.format("Trip: %s %s %s %s\n",locationA.getName(), route.getKey(), duration, locationB.getName());
    }

    @Override
    public String toString(){
        var dep = route.getStopOver(locationA.getName());
        var arr =route.getStopOver(locationB.getName());
        var deptime = dep.getDeparture();
        var arrtime = arr.getArrival();
        Duration duration = Duration.between(deptime,arrtime);
        var locA = locationA.getCoordinates();
        var locB = locationB.getCoordinates();
        var distance = Math.round(locA.getDistance(locB));
    //        return String.format("Trip: %s %s %s %s %s %s\n",locationA.getName(), route.getKey(), dateTime, locationB.getName(), distance,"Km");
        return String.format("Trip: %s %s %s %s %s %s %s %s\n","From:",locationA.getName(), deptime, "To", locationB.getName(),duration, distance,"Km");
    }
}
