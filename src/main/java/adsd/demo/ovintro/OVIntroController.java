package adsd.demo.ovintro;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.util.Callback;
import javafx.util.Duration;

import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class OVIntroController {
    private final ArrayList<Labeled> langSwitchable = new ArrayList<>(); // Arraylist for javafx elements that can change the language
    private final String[] availableLang = {"English", "Netherlands"}; // List of available languages
    private String currentlang = "English"; // current language
    private String kaartImagePath = "Images/beginkaart.png"; // file path to the map png file
    private final double absolutePosition1 = 0.5;
    private final double absolutePosition2 = 0.40;
    private String lastImageLoaded = "Images/beginkaart.png"; // Last image used
    private final Double fontSize = 15.0; // font size for text
    private final Duration showDelay = Duration.millis(0.1); // Delay for how long mouse needs to be hovering over GUI element before tooltip is shown
    private final Font fntVerdana = new Font("Verdana", fontSize); // Font Verdana
    private final Font fntCalibri = new Font("Calibri", fontSize); // Font Calibri
    private final Font fntTimesNewRoman = new Font("Times New Roman", fontSize); // Font Times New Roman
    String[] ovtypes = {"bus", "tram", "train", "plane"}; // All transport types available

    // All FXML elements used in the GUI
    private final ImageView imageView = new ImageView(); // javafx element for showing images
    private final AnchorPane anchorPane = new AnchorPane(); // Anchorpane used for displaying the map images
    @FXML
    private GridPane panels;
    @FXML
    private GridPane topBar;
    @FXML
    private GridPane login;
    @FXML
    private GridPane loggedIn;
    @FXML
    private GridPane favouriteTrips;
    @FXML
    private Pane borderPaneImage;
    @FXML
    private SplitPane splitPane1;
    @FXML
    private SplitPane splitPane2;
    @FXML
    private ComboBox<String> comboTransport;
    @FXML
    private ComboBox<String> comboA;
    @FXML
    private ComboBox<String> comboB;
    @FXML
    private Button buttonSwitch;
    @FXML
    private Button buttonFindTrip;
    @FXML
    private Button buttonSwitchLang;
    @FXML
    private Button buttonAccountNoUser;
    @FXML
    private Button buttonAccountWithUser;
    @FXML
    private Button buttonSignUp;
    @FXML
    private Button buttonLogout;
    @FXML
    private Button buttonFavouriteTrips;
    @FXML
    private Label labelclock;
    @FXML
    private TextField tx_username;
    @FXML
    private TextField tx_password;
    @FXML
    private Label labelUsernameLogin;
    @FXML
    private Label labelUsername;
    @FXML
    private Label labelUsernameLoggedIn;
    @FXML
    private Label labelPassword;
    @FXML
    private DatePicker datePicker;
    @FXML
    private Spinner<Integer> hour;
    @FXML
    private Spinner<Integer> minute;
    @FXML
    private ListView<Trip> listViewOptions;
    @FXML
    private ListView<Trip> listViewFavourites;

    // All FXML Button actions
    @FXML
    public void onComboA() {
        System.out.println("OVIntroController.onComboA");
    }

    @FXML
    public void onComboB() {
        System.out.println("OVIntroController.onComboB");
    }

    @FXML
    protected void onTransport() {
        System.out.println("OVIntroController.onTransportChange");
        putLocations();
    }

    @FXML
    protected void ondatePicker() {
        System.out.print("OVIntroController.ondatePicker");
    }

    @FXML
    protected void onSpinner() {
        System.out.print("OVIntroController.onSpinner");
    }

    //Controller for switch button. Switches A en B
    @FXML
    protected void onButtonSwitch() {
        System.out.println("OVIntroController.onButtonA");
        String selectedA = comboB.getValue();
        comboB.getSelectionModel().select(comboA.getValue());
        comboA.getSelectionModel().select(selectedA);
    }

    @FXML
    protected void onPlanMyTrip() {
        Train train = new Train();
        Tram tram = new Tram();
        Bus bus = new Bus();
        Plane plane = new Plane();

        LocalDate d = datePicker.getValue();
        LocalTime t = LocalTime.of(hour.getValue(), minute.getValue());
        LocalDateTime ldt = LocalDateTime.of(d, t);

        var keyA = comboA.getValue();
        var keyB = comboB.getValue();
        var trips = train.getTrips(keyA, keyB, ldt);
        trips.write();

        ObservableList<Trip> tripsObservableList = FXCollections.observableArrayList();
        for (var tr = trips.iterator(); tr.hasNext();/**/) {
            var mytrip = tr.next();
            mytrip.write();
            tripsObservableList.add(mytrip);
        }
        listViewOptions.setItems(tripsObservableList);
        updateTripListView(listViewOptions);
        putImageFile();
    }

    @FXML
    protected void onButtonLogin() {
        DBUtils.logInUser(tx_username.getText(), tx_password.getText());
        if (DBUtils.loggedIn) {
            buttonAccountNoUser.setDisable(true);
            loggedIn();
        }
    }

    @FXML
    protected void onButtonLogout() {
        if (DBUtils.loggedIn) {
            buttonAccountNoUser.setDisable(false);
            login.setDisable(false);
            login.setVisible(true);
            loggedIn.setDisable(true);
            loggedIn.setVisible(false);
            buttonAccountWithUser.setDisable(true);
            tx_password.setText("");
            tx_username.setText("");
            DBUtils.logoutUser();
            buttonFavouriteTrips.setDisable(true);
            buttonFavouriteTrips.setVisible(false);
            ObservableList<Trip> observableList = FXCollections.observableArrayList();
            listViewFavourites.setItems(observableList);
            favouriteTrips.setVisible(false);
            favouriteTrips.setDisable(true);
        }
    }

    private void loggedIn() {
        loggedIn.setDisable(false);
        loggedIn.setVisible(true);
        login.setDisable(true);
        login.setVisible(false);
        labelUsername.setText(tx_username.getText());
        buttonAccountWithUser.setDisable(false);
        buttonAccountWithUser.fire();
        buttonFavouriteTrips.setDisable(false);
        buttonFavouriteTrips.setVisible(true);
        favouriteTrips.setVisible(true);
        favouriteTrips.setDisable(false);
    }

    @FXML
    protected void onButtonSignUp() {
        if (!tx_username.getText().trim().isEmpty() && !tx_password.getText().trim().isEmpty()) {
            DBUtils.signUpUser(tx_username.getText(), tx_password.getText());
            if (DBUtils.loggedIn) {
                DBUtils.logInUser(tx_username.getText(), tx_password.getText());
                loggedIn();
            }
        } else {
            System.out.println("Please fill in all information");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill in all information to sign up!");
            alert.show();
        }
    }

    @FXML
    protected void onSwitchLang() throws IOException, ParserConfigurationException, SAXException {
        FileHandler fileHandler = new FileHandler();
        for (String s : availableLang) {
            if (!s.equals(currentlang)) {
                currentlang = s;
                break;
            }
        }

        for (Labeled label : langSwitchable) {
            String newText = fileHandler.getObjectText(label.getId(), currentlang);
            label.setText(newText);
        }

        ArrayList<String> newOvtypes = new ArrayList<>();
        for (String s : ovtypes) {
            newOvtypes.add(fileHandler.getObjectText(s, currentlang));
        }

        int itemIndex = comboTransport.getItems().indexOf(comboTransport.getValue());
        ObservableList<String> list = FXCollections.observableArrayList(newOvtypes);
        comboTransport.setItems(list);
        comboTransport.setValue(list.get(itemIndex));
    }

    // Slide animation for Panes using a button
    @FXML
    private void prepareSlideMenuAnimation(GridPane gridPane, Button button) {
        TranslateTransition openLogin = new TranslateTransition(new Duration(350), gridPane);
        openLogin.setToY(0);
        TranslateTransition closeLogin = new TranslateTransition(new Duration(350), gridPane);
        button.setOnAction((ActionEvent evt) -> {
            if (gridPane.getTranslateY() != 0) {
                openLogin.play();
            } else {
                closeLogin.setToY((gridPane.getHeight()));
                closeLogin.play();
            }
        });
    }

    // Important method to initialize this Controller object!!!
    public void initialize() {
        // prepare all Panes that need a slide animation
        prepareSlideMenuAnimation(login, buttonAccountNoUser);
        prepareSlideMenuAnimation(loggedIn, buttonAccountWithUser);
        prepareSlideMenuAnimation(favouriteTrips, buttonFavouriteTrips);

        // Initiate a clock for the GUI
        initClock();
        initSpinner();

        // Set all constraints for the gui for correct scaling
        {
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(33);

            RowConstraints row = new RowConstraints();
            row.setPercentHeight(15);

            panels.getColumnConstraints().add(column);
            panels.getRowConstraints().add(row);

            row = new RowConstraints();
            row.setPercentHeight(85);
            panels.getRowConstraints().add(row);

            row = new RowConstraints();
            row.setPercentHeight(100);
            loggedIn.getRowConstraints().add(row);

            column = new ColumnConstraints();
            column.setPercentWidth(10);
            topBar.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(40);
            topBar.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(25);
            topBar.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(25);
            topBar.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(25);
            loggedIn.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(25);
            loggedIn.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(50);
            loggedIn.getColumnConstraints().add(column);

            column = new ColumnConstraints();
            column.setPercentWidth(100);
            favouriteTrips.getColumnConstraints().add(column);

            panels.setPrefSize(1000, 600); // Default width and height
            panels.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        }

        // Initialise the combo box comboTransport with transportation types ...
        {
            ObservableList<String> list = FXCollections.observableArrayList(ovtypes);
            comboTransport.setItems(list);
            comboTransport.getSelectionModel().select(2); // i.e. "train"
        }

        // Initialise the combo box comboA with stopover locations.
        {
            putLocations();
        }

        // Set the parameters for the splitpane
        {
            splitPane1.getDividers().get(0).positionProperty().addListener((observable, oldValue, newValue) -> {
                splitPane1.setDividerPosition(0, absolutePosition1);
            });
            splitPane2.getDividers().get(0).positionProperty().addListener((observable, oldValue, newValue) -> {
                splitPane2.setDividerPosition(0, absolutePosition2);
            });
        }

        //initiates tooltips on buttons
        {
            //Switch button
            Tooltip tts = new Tooltip();
            tts.setText("Switch the start and end location");
            tts.setFont(fntTimesNewRoman);
            tts.setShowDelay(showDelay);
            buttonSwitch.setTooltip(tts);
            //Language switch button
            Tooltip ttl = new Tooltip();
            ttl.setText("Switch the language");
            ttl.setFont(fntVerdana);
            ttl.setShowDelay(showDelay);
            buttonSwitchLang.setTooltip(ttl);
            //confirm button
            Tooltip ttc = new Tooltip();
            ttc.setText("Confirm choices");
            ttc.setFont(fntVerdana);
            ttc.setShowDelay(showDelay);
            buttonFindTrip.setTooltip(ttc);
            //Choice A
            Tooltip tta = new Tooltip();
            tta.setText("Choose Start location");
            tta.setFont(fntVerdana);
            tta.setShowDelay(showDelay);
            comboA.setTooltip(tta);
            //choice B
            Tooltip ttb = new Tooltip();
            ttb.setText("Choose end location");
            ttb.setFont(fntVerdana);
            ttb.setShowDelay(showDelay);
            comboB.setTooltip(ttb);
            //TransportType
            Tooltip ttt = new Tooltip();
            ttt.setText("Choose transport type");
            ttt.setFont(fntVerdana);
            ttt.setShowDelay(showDelay);
            comboTransport.setTooltip(ttt);
        }

        // var data = new Data();
        drawImage();
        imageView.setCache(false);
        anchorPane.getChildren().add(imageView);
        borderPaneImage.getChildren().add(anchorPane);

        // Initiation for the language switching, adds all the GUI elements that need language switching to an Arraylist
        langSwitchable.add(buttonSwitch);
        langSwitchable.add(buttonFindTrip);
        langSwitchable.add(buttonSwitchLang);
        langSwitchable.add(buttonSignUp);
        langSwitchable.add(labelUsernameLogin);
        langSwitchable.add(labelUsernameLoggedIn);
        langSwitchable.add(labelPassword);
        langSwitchable.add(buttonLogout);

        // Set the actions for clicking in the Options listview
        listViewOptions.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (DBUtils.loggedIn) {
                    ObservableList<Trip> favourites = listViewFavourites.getItems();
                    if(event.getClickCount() == 2) {
                        favourites.add(listViewOptions.getSelectionModel().getSelectedItem());
                        listViewFavourites.setItems(favourites);
                        updateTripListView(listViewFavourites);
                        System.out.println("Added favourite");
                    }
                }
            }
        });

        // Set the actions for clicking in the Favourites listview
        listViewFavourites.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (DBUtils.loggedIn) {
                    ObservableList<Trip> favourites = listViewFavourites.getItems();
                    if (event.getClickCount() == 2) {
                        favourites.remove(listViewFavourites.getSelectionModel().getSelectedIndex());
                        updateTripListView(listViewFavourites);
                    }
                }
            }
        });

        System.out.println("init done");
    }

    // Update the Listview with the current values
    private void updateTripListView(ListView<Trip> listView) {
        listView.setCellFactory(new Callback<>() {
            @Override
            public ListCell<Trip> call(ListView<Trip> tripListView) {
                return new ListCell<>() {
                    @Override
                    public void updateItem(Trip trip, boolean empty) {
                        super.updateItem(trip, empty);
                        if (!empty) {
                            setText(trip.toString());
                        }
                    }
                };
            }
        });
    }

    // Initiate the clock for the GUI
    private void initClock() {
        Timeline clock = new Timeline(new KeyFrame(javafx.util.Duration.ZERO, event -> {
            DateTimeFormatter dft = DateTimeFormatter.ofPattern("HH:mm:ss");
            labelclock.setText(LocalTime.now().format(dft));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    //
    private void initSpinner() {
        hour.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 24)
        );
        minute.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 60)
        );
    }

   //get the right file path for the image for all routes
   private void putImageFile() {
        String keuzeA = comboA.getSelectionModel().getSelectedItem().replaceAll(" ", "").toLowerCase();
        System.out.println(keuzeA);
        String keuzeB = comboB.getSelectionModel().getSelectedItem().replaceAll(" ", "").toLowerCase();
        System.out.println(keuzeB);
        String imagePathAB = keuzeA+"-" + keuzeB +".png";
        String imagePathBA = keuzeB+"-" + keuzeA +".png";
        File dir = new File("Images");
        String[] allImages = dir.list();
        assert allImages != null;
        for(Object img : allImages)
        {
            System.out.println(img.toString());
            if(img.equals(imagePathAB.toLowerCase().strip().trim()))
            {
                kaartImagePath = "Images/"+imagePathAB;
            }
            if(img.equals(imagePathBA))
            {
                kaartImagePath = "Images/"+imagePathBA;
            }
        }
        drawImage();
   }

   // Removes the last image
   public void clearImage() {
        lastImageLoaded = kaartImagePath;
        borderPaneImage.getChildren().remove(anchorPane);
        anchorPane.getChildren().remove(imageView);
        imageView.imageProperty().setValue(null);
        anchorPane.getChildren().add(imageView);
        borderPaneImage.getChildren().add(anchorPane);
        drawImage();
   }

    //initiates the image and makes it display on screen
    public void drawImage() {
        FileInputStream reader;
        try {
            reader = new FileInputStream(kaartImagePath);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        if (lastImageLoaded.equals(kaartImagePath)) {
            Image image = new Image(reader);
            imageView.setImage(image);
            imageView.fitHeightProperty().bind(splitPane1.heightProperty().divide(2));
            imageView.setPreserveRatio(true);
        } else {
            clearImage();
        }
    }

    // Get all routes from the correct transport type
    public void putLocations() {
        ArrayList<String> locations = new ArrayList<>();
        if (comboTransport.getSelectionModel().getSelectedItem() != null) {
            if (comboTransport.getSelectionModel().getSelectedItem().equals("train") || comboTransport.getSelectionModel().getSelectedItem().equals("Train") || comboTransport.getSelectionModel().getSelectedItem().equals("train") || comboTransport.getSelectionModel().getSelectedItem().equals("Train")) {
                ArrayList<String> trainLocations = new ArrayList<>();
                Train train = new Train();
                for (Location l : train.getLocationMap().values()) {
                    trainLocations.add(l.getName());
                }
                locations = trainLocations;
            }

            if (comboTransport.getSelectionModel().getSelectedItem().equals("bus")) {
                ArrayList<String> busLocations = new ArrayList<>();
                Bus bus = new Bus();
                for (Location l : bus.getLocationMap().values()) {
                    busLocations.add(l.getName());
                }
                locations = busLocations;
            }
        }

        if (comboTransport.getSelectionModel().getSelectedItem().equals("plane")) {
            ArrayList<String> planeLocations = new ArrayList<>();
            Plane plane = new Plane();
            for (Location loc : plane.getLocationMap().values()) {
                planeLocations.add(loc.getName());
            }
            locations = planeLocations;
        }

        if (comboTransport.getSelectionModel().getSelectedItem().equals("tram")) {
            ArrayList<String> planeLocations = new ArrayList<>();
            Tram tram = new Tram();
            for (Location loc : tram.getLocationMap().values()) {
                planeLocations.add(loc.getName());
            }
            locations = planeLocations;
        }

        ObservableList<String> list = FXCollections.observableArrayList(locations);
        comboA.setItems(list);
        comboA.getSelectionModel().select(0); // i.e. "Amsterdam"
        comboB.setItems(list);
        comboB.getSelectionModel().select(comboB.getItems().size() - 1);
    }
}
