package adsd.demo.ovintro;

import javafx.scene.control.Alert;
import java.sql.*;

public class DBUtils {
    // Boolean if a user is logged in
    public static boolean loggedIn = false;

    // Connect to database and check if login details are correct/available, if so, store login details in database
    public static void signUpUser(String username, String password) {
        Connection connection = null;
        PreparedStatement psInsert = null;
        PreparedStatement psCheckUserExists = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://162.241.24.170/definjj6_Team3", "definjj6_HU", "PleaseHelpL0gin");
            psCheckUserExists = connection.prepareStatement("SELECT * FROM users WHERE username = ?");
            psCheckUserExists.setString(1, username);
            resultSet = psCheckUserExists.executeQuery();

            if (resultSet.isBeforeFirst()) {
                System.out.println("User already exists!");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("You cannot use this username");
                alert.show();
            } else {
                EncryptionHandler encryptionHandler = new EncryptionHandler();
                String[] encryption = encryptionHandler.encode(password, encryptionHandler.randomSalt());

                psInsert = connection.prepareStatement("INSERT INTO users (username, password, salt) VALUES (?, ?, ?)");
                psInsert.setString(1, username);
                psInsert.setString(2, encryption[0]);
                psInsert.setString(3, encryption[1]);
                psInsert.executeUpdate();
                loggedIn = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResultSet(resultSet);
            closeCheckUserExists(psCheckUserExists);
            closeInsert(psInsert);
            closeConnection(connection);
        }
    }

    // Logout the current user
    public static void logoutUser() {
        loggedIn = false;
    }

    // Connect to database and check if login details exist/are correct, if so, continue login for user
    public static void logInUser(String username, String password) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://162.241.24.170/definjj6_Team3", "definjj6_HU", "PleaseHelpL0gin");
            preparedStatement = connection.prepareStatement("SELECT  password, salt FROM users WHERE username = ?");
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                System.out.println("User not found in database!");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Provided credentials are incorrect!");
                alert.show();
            } else {
                while (resultSet.next()) {
                    String retrievedPassword = resultSet.getString("password");
                    String retrievedSalt = resultSet.getString("salt");
                    EncryptionHandler encryptionHandler = new EncryptionHandler();
                    if (encryptionHandler.checkPassword(password, retrievedSalt,retrievedPassword)) {
                        loggedIn = true;
                    } else {
                        System.out.println("Passwords did not match!");
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("The Provided credentials are incorrect!");
                        alert.show();
                    }
                }
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            closeResultSet(resultSet);
            closePreparedStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    // Close Resultset
    private static void closeResultSet(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Close Prepared Statement
    private static void closePreparedStatement(PreparedStatement preparedStatement) {
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Close Connection
    private static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Close Prepared Statement CheckUserExists
    private static void closeCheckUserExists(PreparedStatement psCheckUserExists) {
        if (psCheckUserExists != null) {
            try {
                psCheckUserExists.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Close Prepared Statement Insert
    private static void closeInsert(PreparedStatement psInsert) {
        if (psInsert != null) {
            try {
                psInsert.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}